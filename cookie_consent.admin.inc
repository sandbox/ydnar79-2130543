<?php

function cookie_consent_settings_form($form, &$form_state)
{
  $form['cookie_consent_style'] = array(
    '#title' => t('Style'),
    '#type' => 'select',
    '#options' => array('light' => t('Light'), 'dark' => t('Dark'), 'monochrome' => t('Monochrome')),
    '#default_value' => variable_get('cookie_consent_style',COOKIE_CONSENT_STYLE)
  );

  $form['cookie_consent_banner_position'] = array(
    '#title' => t('Primary banner position'),
    '#description' => t('A string denoting which style to use.'),
    '#type' => 'select',
    '#options' => array('top' => t('Top'), 'bottom' => t('Bottom'), 'push' => t('Push from top (expirimental)')),
    '#default_value' => variable_get('cookie_consent_banner_position',COOKIE_CONSENT_BANNER_POSITION)
  );

  $form['cookie_consent_tag_position'] = array(
    '#title' => t('Privacy Settings tab position'),
    '#description' => t('The position of the \'privacy settings\' tab.'),
    '#type' => 'select',
    '#options' => array(
      'bottom-right' => t('Bottom right'),
      'bottom-left' => t('Bottom left'),
      'vertical-left' => t('Vertical left'),
      'vertical-right' => t('Vertical right')
    ),
    '#default_value' => variable_get('cookie_consent_tag_position',COOKIE_CONSENT_TAG_POSITION)
  );

  $form['cookie_consent_consent_type'] = array(
    '#title' => t('Consent type'),
    '#description' => t('Should the plugin use explicit or implied consent.'),
    '#type' => 'select',
    '#options' => array(
      'explicit' => t('Explicit - no cookies will be set until a visitor consents'),
      'implicit' => t('Implied - set cookies and allow vistors to opt out')
    ),
    '#default_value' => variable_get('cookie_consent_consent_type',COOKIE_CONSENT_CONSENT_TYPE),
  );

  $form['cookie_consent_refresh_on_consent'] = array(
    '#title' => t('Refresh on consent'),
    '#description' => t('When set to true, the plugin will refresh the page after a change in consent.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('cookie_consent_refresh_on_consent',COOKIE_CONSENT_REFRESH_ON_CONSENT)
  );

  $form['cookie_consent_use_ssl'] = array(
    '#title' => t('Use SSL'),
    '#description' => t('Whether or not the plugin should use SSL'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('cookie_consent_use_ssl',COOKIE_CONSENT_USE_SSL)
  );

  $form['cookie_consent_only_show_banner_once'] = array(
    '#prefix' => '<div id="showbanneronlyonce_wrapper">',
    '#suffix' => '</div>',
    '#title' => t('Only show banner once'),
    '#description' => t('This option can only be used when consenttype is set to "implicit". The consent slide-down notification will only be shown once if set to true even if the visitor does not respond to the banner (on subsequent pages, just the privacy settings tab will be shown).'),
    '#type' => 'checkbox',
	'#states' => array(
  		'visible' => array(
		    ':input[name="cookie_consent_consent_type"]' => array('value' => 'implicit'),
		  ),
	),
    '#default_value' => variable_get('cookie_consent_only_show_banner_once',COOKIE_CONSENT_ONLY_SHOW_BANNER_ONCE)
  );

  $form['cookie_consent_hide_all_sites_button'] = array(
    '#title' => t('Hide all sites button'),
    '#description' => t('The "Save for all sites"/"Accept for all sites" button can be hidden.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('cookie_consent_hide_all_sites_button',COOKIE_CONSENT_HIDE_ALL_SITES_BUTTON)
  );

  $form['cookie_consent_disable_all_sites'] = array(
    '#title' => t('Disable all sites'),
    '#description' => t('This disables all of the "all sites" features within the plugin. Visitors will have to explicitly give consent on your site.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('cookie_consent_disable_all_sites',COOKIE_CONSENT_DISABLE_ALL_SITES)
  );

  $form['cookie_consent_hide_privacy_settings_tab'] = array(
    '#title' => t('Hide privacy settings tab'),
    '#description' => t('The privacy tab can be hidden.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('cookie_consent_hide_privacy_settings_tab',COOKIE_CONSENT_HIDE_PRIVACY_SETTINGS_TAB)
  );

  $form['cookie_consent_testmode'] = array(
    '#title' => t('Testmode'),
    '#description' => t('The plugin can be forced into test mode, accepting all cookies or declining all cookies (not for production use).'),
    '#type' => 'select',
    '#options' => array(
      'false' => t('Off'),
      'accept' => t('Accept'),
      'decline' => t('Decline')
    ),
    '#default_value' => variable_get('cookie_consent_testmode',COOKIE_CONSENT_TESTMODE)
  );

  $form['cookie_consent_override_warnings'] = array(
    '#title' => t('Override warnings'),
    '#description' => t('By default, the plugin will warn the developer if no code blocks are found. This is to aid developers when setting up the plugin, but it can also be normal (e.g. the plugin is added to all pages, but one page of the website doesn\'t have any cookie setting scripts)'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('cookie_consent_override_warnings',COOKIE_CONSENT_OVERRIDE_WARNINGS)
  );

  $form['cookie_consent_ignore_do_not_track'] = array(
    '#title' => t('Ignore Do Not Track cookie'),
    '#description' => t('By default, the plugin will check for the Do Not Track browser standard. If this is found, it will default to explicit consent mode and only set cookies given the express permission of the visitor. By changing this setting to true, the plugin will not perform this check and use your chosen consent method regardless.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('cookie_consent_ignore_do_not_track',COOKIE_CONSENT_IGNOREDONOTTRACK)
  );

  $form['experimental'] = array(
    '#type' => 'fieldset',
    '#title' => t('Experimental Features'),
	'#collapsible' => TRUE, 
    '#collapsed' => TRUE,
  );
  $form['experimental']['cookie_consent_only_show_within_eu'] = array(
    '#title' => t('Only show in the EU'),
    '#description' => t('The plugin can be set to only show the banner within the EU. This option requires an API key and must be used in conjunction with IPInfoDB API key (below).'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('cookie_consent_only_show_within_eu',COOKIE_CONSENT_ONLYSHOWWITHINEU)
  );

  $form['experimental']['cookie_consent_ipinfodb_key'] = array(
    '#type' => 'textfield',
    '#title' => t('IPInfoDB API Key'),
    '#description' => t('In order to tell if the visitor is within the EU, the plugin needs to contact a third party database to locate them based on their IP. For this purpose, Cookie Consent currently only supports IPInfoDB.  Before you can use their service, IPInfoDB require that you sign up on their website and obtain an API key which uniquely identifies the location look-ups that your website makes.  You can obtain a key (usually free of charge depending on the volume of visitors you receive) from <a target="_blank" href="http://www.ipinfodb.com">www.IPInfoDB.com</a>.  When this option is enabled, Cookie Consent will contact IPInfoDB once for each visitor, on their first page view (we then store the visitors location to reduce the number of calls to IPInfoDB). IPInfoDB claim an accuracy of 99.5%. It is important to note that Silktide and Cookie Consent are in no way affiliated with IPInfoDB, and provide no warranty or endorsement of their service.  You should carefully read IPInfoDBs terms and conditions and fair usage policies before using their service.  This option only works when onlyshowwithineu is set to true (above).'),
	'#size' => 60, 
	'#maxlength' => 128, 
    '#default_value' => variable_get('cookie_consent_ipinfodb_key', COOKIE_CONSENT_IPINFODBKEY),
	'#states' => array(
  		'visible' => array(
		    ':input[name="cookie_consent_only_show_within_eu"]' => array('checked' => TRUE),
		  ),
  		'required' => array(
		    ':input[name="cookie_consent_only_show_within_eu"]' => array('checked' => TRUE),
		  ),
	),

  );


  if(isset($form_state['values']['cookie_consent_consent_type']) && $form_state['values']['cookie_consent_consent_type'] == 'implicit')
  {
    $form['cookie_consent_onlyshowbanneronce']['#disabled'] = true;
  }

  if($form_state['values']['cookie_consent_only_show_within_eu'] == FALSE)
  {
    $form['cookie_consent_ipinfodb_key']['#disabled'] = true;
  }

	// Add a custom submit handler to allow for a bit of custom code to be run on configuration updates.
	$form['#submit'][] = 'cookie_consent_settings_form_submit';

  return system_settings_form($form);
}

/**
 * Custom page submit handler().
 * This allows for a few custom adjustments before the default submit handler can process the form.
 */
function cookie_consent_settings_form_submit($form, &$form_state) {

 // Clear obsolete local cache on save so that updated info is used.
    cookie_consent_clear_js_cache();

}


/**
 * Delete cached files and directory.
 */
function cookie_consent_clear_js_cache() {             
  $path = 'public://cookie_consent';
  if (file_prepare_directory($path)) {
    file_scan_directory($path, '/.*/', array('callback' => 'file_unmanaged_delete'));
    drupal_rmdir($path);

    // Change query-strings on css/js files to enforce reload for all users.
    _drupal_flush_css_js();

    watchdog('cookie consent', 'Local cache has been purged.', array(), WATCHDOG_INFO);
  }
}

/**
 * @param $form
 * @param $form_state
 * @param null $id
 * @return array
 */
function cookie_consent_groups_add_form($form, &$form_state, $id = NULL)
{
  $form = array();

  if($id)
  {
    $result = db_query('SELECT * FROM {cookie_consent_groups} WHERE ccgid = :ccgid',array(':ccgid' => $id));
    $group = $result->fetchObject();

    $form['ccgid'] = array(
      '#type' => 'hidden',
      '#value' => $id
    );

    drupal_set_title(t('Edit group '.$group->title));
  }

  $form['id'] = array(
    '#title' => t('ID'),
    '#type' => 'textfield',
    '#required' => true,
    '#size' => 50,
    '#description' => t('Unique ID to identify the cookie group. Example: social OR analytics'),
    '#default_value' => isset($group) ? $group->id : ''
  );

  $form['title'] = array(
    '#title' => t('Title'),
    '#type' => 'textfield',
    '#required' => true,
    '#description' => t('Title to discribe group for website visitior'),
    '#default_value' => isset($group) ? $group->title : ''
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#required' => true,
    '#description' => t('Description about group for website visitior'),
    '#default_value' => isset($group) ? $group->description : ''
  );

  $form['active'] = array(
    '#title' => t('Enabled'),
    '#type' => 'checkbox',
    '#default_value' => isset($group) ? $group->active : ''
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );

  if($group->locked)
  {
    $form['id']['#disabled'] = true;
  }

  return $form;
}

/**
 * @param $form
 * @param $form_state
 * @param $id
 */
function cookie_consent_groups_add_form_validate($form, &$form_state, $id = NULL)
{
  // If the ID was not passed directly, get it from the form value.
  if(empty($id)) {
	  $id = $form_state['values']['id'];
  }

  // check for unique id
  $result = db_query('SELECT SUM(ccgid) as count FROM {cookie_consent_groups} WHERE id = :id',array(':id' => $form_state['values']['id']));
  $row = $result->fetchObject();

  if(isset($form_state['values']['ccgid']))
  {
    $original = cookie_consent_group($form_state['values']['ccgid']);

    if($original->id != ($form_state['values']['id']) && isset($row->count) && $row->count > 0)
    {
      form_set_error('id',t('ID needs to be unique'));
    }
  }
  elseif(isset($row->count) && $row->count > 0)
  {
    form_set_error('id',t('ID needs to be unique'));
  }
}

/**
 * @param $form
 * @param $form_state
 * @param $id
 */
function cookie_consent_groups_add_form_submit($form, &$form_state, $id = NULL)
{
  if(isset($form_state['values']['ccgid']))
  {
    $group = cookie_consent_group($form_state['values']['ccgid']);
    if($group->locked)
    {
      unset($form_state['values']['id']);
    }

    drupal_write_record('cookie_consent_groups',$form_state['values'],'ccgid');
    drupal_set_message(t('Updated group :group',array(':group' => $form_state['values']['title'])));
  }
  else
  {
    drupal_write_record('cookie_consent_groups',$form_state['values']);
    drupal_set_message(t('Created group :group',array(':group' => $form_state['values']['title'])));
  }

  drupal_goto('admin/config/system/cookie_consent/groups');
}


/**
 * @param $form
 * @param $form_state
 * @param $id
 * @return bool
 */
function cookie_consent_groups_delete_form($form, &$form_state, $id)
{
  if(!isset($id))
  {
    return false;
  }

  $result = db_query('SELECT * FROM {cookie_consent_groups} WHERE ccgid = :id',array(':id' => $id));
  $edit = $result->fetchObject();

  drupal_set_title(t('Are you sure you want to delete group @group',array('@group' => $edit->title)));

  $form['ccgid'] = array(
    '#type' => 'hidden',
    '#value' => $id
  );

  $form['markup'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('This operation cannot be reversed.  Are you sure you want to delete group @group',array('@group' => $edit->title)) . '? </p>'
  );

  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Yes, delete'),
    '#name' => 'delete'
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('No, cancel'),
    '#name' => 'cancel'
  );

  return $form;
}

/**
 * @param $form
 * @param $form_state
 * @return bool
 */
function cookie_consent_groups_delete_form_submit($form, &$form_state)
{

  if(!isset($form_state['values']['ccgid']))
  {
    return false;
  }

  $result = db_query('SELECT * FROM {cookie_consent_groups} WHERE ccgid = :id',array(':id' => $form_state['values']['ccgid']));
  $edit = $result->fetchObject();

  if($form_state['clicked_button']['#name'] == 'delete')
  {
    $result = db_query('DELETE FROM {cookie_consent_groups} WHERE ccgid = :id',array(':id' => $form_state['values']['ccgid']));
    drupal_set_message(t('Deleted group :group',array(':group' => $edit->title)));
  }

  drupal_goto('admin/config/system/cookie_consent/groups');
}

function cookie_consent_groups_switch_status($id,$status)
{
  $group = cookie_consent_group($id);

  if($status == 'enable')
  {
    $group->active = 1;
  }
  else
  {
    $group->active = 0;
  }

  drupal_write_record('cookie_consent_groups',$group,'ccgid');
  drupal_goto('admin/config/system/cookie_consent/groups');
}

/**
 * @return string
 */
function cookie_consent_group_overview()
{
  $groups = cookie_consent_groups();
  $table = array(
    'header' => array(t('ID'),t('Title'),t('Description'),''),
    'rows' => array(),
    'attributes' => array(),
    'caption' => '',
    'colgroups' => array(),
    'sticky' => false,
    'empty' => false
  );

  foreach($groups as $group)
  {
    $edit_links = l('edit','admin/config/system/cookie_consent/groups/'.$group->ccgid.'/edit');

    if($group->active)
    {
      $edit_links .= ' - '.l('disable','admin/config/system/cookie_consent/groups/'.$group->ccgid.'/disable');
    }
    else {
      $edit_links .= ' - '.l('enable','admin/config/system/cookie_consent/groups/'.$group->ccgid.'/enable');
    }

    if(!$group->locked)
    {
      $edit_links .= ' - '.l('delete','admin/config/system/cookie_consent/groups/'.$group->ccgid.'/delete');
    }
     $table['rows'][] = array(
       $group->id,
       $group->title,
       $group->description,
       $edit_links
     );
  }

  $content = theme_table($table);
  return $content;
}

