
-- SUMMARY --

This module allows for easy integration and configuration of the Content Consent cookie solution from Silkbeam.

NOTE: I am not associated with Silkbeam in any way.

This module includes:

Core module: Allows for easy configuration of Cookie Consent option through Drupal.

Integration with other modules: Addthis, Adsense, FBLikeButton and Google Analytics.

I will add more options in the future as time permits. Any suggestions / commits are greatly appreciated.



-- INSTALLATION --

Normal Drupal module installation, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

Use the admin configuration page to configure settings.  Configuration path: /admin/config/system/cookie_consent


-- CONTACT --

Current maintainers:
* Randy Timmerman (ydnar79) - http://drupal.org/user/286387
