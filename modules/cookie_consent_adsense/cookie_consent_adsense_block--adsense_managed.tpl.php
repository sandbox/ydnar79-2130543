<?php
/**
 * @file
 * Template file for google adsense.
 *
 */
?>

<script type="text/plain" class="cc-onconsent-advertising">

jQuery('#ccAdsense-<?php print $block_html_id; ?>').html('<div id="jcv <?php print str_replace('adsense', '', $block_html_id); ?>" class="<?php print str_replace('adsense', '', $classes); ?>"<?php print $attributes; ?>>\
<?php print render($title_prefix); ?>\
<?php if ($block->subject): ?>
  <h2<?php print $title_attributes; ?>><?php print $block->subject ?></h2>\
<?php endif;?>
  <?php print str_replace("\n", "", render($title_suffix)); ?> \
  <div class="content"<?php print str_replace("\n", "", str_replace("'", "&#39;'", $content_attributes)); ?>> \
    <?php print(str_replace("\n", "", str_replace("'", "\'", $content))); ?>
 \
  </div>\
</div>');

</script>

<div id="ccAdsense-<?php print $block_html_id; ?>"></div>