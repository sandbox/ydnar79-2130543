(function ($) {


  Drupal.behaviors.cookie_consent_addthis = {
    attach: function(context, settings) {
	cc.onconsent('social', function(){
      $.getScript(
        Drupal.settings.addthis.widget_url,
        function(data, textStatus) {
          addthis.init();
        }
      );
      if (context != window.document && window.addthis != null) {
        window.addthis.ost = 0;
        window.addthis.ready();
      }
	});
   }

  };



}(jQuery));