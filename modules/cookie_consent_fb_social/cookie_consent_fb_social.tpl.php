<?php
/**
 * @file fb-social-plugin.tpl.php
 * Theme the more link
 *
 * - $plugin_type: the type of this plugin
 * - $tag_name : the tagname of this plugin
 */
?>
<script type="text/plain" class="cc-onconsent-social">

jQuery('#ccFBSocial-<?php print $plugin_type?>').html('<div class="fb-social-<?php print $plugin_type?>-plugin"> \
  <<?php print $tag_name ?> <?php print drupal_attributes($options)?>></<?php print $tag_name?>> \
</div>');
</script>

<div id="ccFBSocial-<?php print $plugin_type?>"></div>



